import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from '@layout/layout.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: '/',
        pathMatch: 'full',
      },
      {path: '', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)},
      {path: 'cv-view', loadChildren: () => import('./pages/cv-view/cv-view.module').then(m => m.CvViewModule)},
    ]
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
