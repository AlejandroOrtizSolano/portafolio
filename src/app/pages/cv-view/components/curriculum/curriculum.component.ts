import {Component, OnInit} from '@angular/core';
import {jsPDF} from 'jspdf';

import * as html2canvas from 'html2canvas';


@Component({
  selector: 'profile-curriculum',
  templateUrl: './curriculum.component.html',
  styleUrls: ['./curriculum.component.scss']
})
export class CurriculumComponent implements OnInit {


  constructor() {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.generarPDF();
    }, 600);
  }

  public generarPDF(): void {
    html2canvas(document.getElementById('card-cv')).then((canvas) => {
      document.body.appendChild(canvas);
    });
    html2canvas(document.getElementById('card-cv'), {
      // Opciones
      allowTaint: true,
      useCORS: false,
      // Calidad del PDF
      scale: 1
    }).then((canvas) => {
      const img = canvas.toDataURL('image/png');
      const doc = new jsPDF();
      doc.addImage(img, 'PNG', 5, 5, 195, 105);
      doc.save('postres.pdf');
    });
  }
}
