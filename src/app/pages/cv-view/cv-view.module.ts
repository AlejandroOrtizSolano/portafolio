import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CvViewRoutingModule } from './cv-view-routing.module';
import { CvViewComponent } from './cv-view.component';
import {ComponentsModule} from '@pages/cv-view/components/components.module';


@NgModule({
  declarations: [CvViewComponent],
  imports: [
    CommonModule,
    CvViewRoutingModule,
    ComponentsModule
  ]
})
export class CvViewModule { }
