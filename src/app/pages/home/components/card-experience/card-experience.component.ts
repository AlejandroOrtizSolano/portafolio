import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'profile-card-experience',
  templateUrl: './card-experience.component.html',
  styleUrls: ['./card-experience.component.scss']
})
export class CardExperienceComponent implements OnInit {
  @Input() type: string;

  // public url:  = `../../../assets/icons/${this.type}.svg`;
  constructor() { }

  ngOnInit(): void {
  }

}
