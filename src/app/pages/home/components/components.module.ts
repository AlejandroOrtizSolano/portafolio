import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardExperienceComponent } from './card-experience/card-experience.component';
import { ProjectsComponent } from './projects/projects.component';
import { CvWindowComponent } from './cv-window/cv-window.component';
import { ProjectsMobilComponent } from './projects-mobil/projects-mobil.component';



@NgModule({
  declarations: [CardExperienceComponent, ProjectsComponent, CvWindowComponent, ProjectsMobilComponent],
  imports: [
    CommonModule
  ],
  exports: [
    CardExperienceComponent,
    ProjectsComponent,
    CvWindowComponent,
    ProjectsMobilComponent
  ]
})
export class ComponentsModule { }
