import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsMobilComponent } from './projects-mobil.component';

describe('ProjectsMobilComponent', () => {
  let component: ProjectsMobilComponent;
  let fixture: ComponentFixture<ProjectsMobilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectsMobilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsMobilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
