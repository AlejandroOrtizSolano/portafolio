import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'profile-projects-mobil',
  templateUrl: './projects-mobil.component.html',
  styleUrls: ['./projects-mobil.component.scss']
})
export class ProjectsMobilComponent implements OnInit {

  public videoName: string[] = [
    'mobil-kb.mp4',
    'mobil-srv.mp4'
  ];
  public videoIndex = 0;
  constructor() { }

  ngOnInit(): void {
  }

}
