import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'profile-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  public intranetACN = [
    'acn-intranet.png',
    'acn-intranet2.png',
    'acn-system.png',
  ];
  public progress: boolean;
  public contador = 0;

  constructor() {
  }

  ngOnInit(): void {
    setInterval((args) => {
      console.log(this.contador, this.intranetACN.length);
      if (this.contador < this.intranetACN.length - 1) {
        this.contador++;
      } else {
        this.contador = 0;
      }

    }, 8000);
    this.progress = true;
  }

}
