import {Component, OnInit} from '@angular/core';
import Typed from 'typed.js';

@Component({
  selector: 'profile-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public typed: any;

  constructor() {
  }

  ngOnInit(): void {
    const options = {
      strings: ['<i>First</i> sentence.', '&amp; a second sentence.'],
      typeSpeed: 40
    };
    const element = document.getElementById('typed');
    this.typed = new Typed(element, {
      strings: [
        'Angular',
        'Ionic',
        'Java',
        'Spring Boot',
        'TypeScript',
        'JavaScript',
        'SQL Server',
      ],
      typeSpeed: 45,
      backSpeed: 45,
      backDelay: 1300,
      // smartBackspace: true,
      loop: true,

    });
  }

}
